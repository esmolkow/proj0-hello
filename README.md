# Proj0-Hello
-------------

Trivial project to print "Hello world" in Python.

Author: Unknown, modified by Elyse Smolkowski

Contact: esmolkow@uoregon.edu

## Usage:
----------

- Copy credentials-skel.ini to credentials.ini, and move to 'hello' directory.

- Fill in credentials.ini with credentials.

- Use command ``make run`` to execute.
